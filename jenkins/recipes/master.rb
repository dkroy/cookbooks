include_recipe 'jenkins::prereqs'

bash 'add_jenkins_repo' do
  cwd "/tmp"
  code <<-EOH
    wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | apt-key add -
    echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list
    apt-get update -y
  EOH
end

bash 'install_jenkins' do
  cwd "/tmp"
  code <<-EOH
    apt-get -y install jenkins jenkins-cli
  EOH
end

directory '/home/jenkins' do
    mode '0777'
    owner 'jenkins'
    group 'jenkins'
    action :create
    recursive true
end

directory '/var/lib/jenkins/.m2' do
    mode '0777'
    owner 'jenkins'
    group 'jenkins'
    action :create
    recursive true
end

cookbook_file '/etc/default/jenkins' do
    source 'etc/default/jenkins'
    mode 0755
end

bash 'move_jenkins' do
  cwd '/tmp'
  code <<-EOH
  mkdir /mnt/var/lib
  /etc/init.d/jenkins stop
  mv /var/lib/jenkins /mnt/var/lib
  /etc/init.d/jenkins start
  EOH
end

link '/etc/nginx/sites-enabled/default' do
    action :delete
    only_if "test -L /etc/nginx/sites-enabled/default"
end

cookbook_file '/etc/nginx/sites-available/jenkins-proxy' do
    source 'nginx/jenkins-proxy'
    mode 0755
end

link '/etc/nginx/sites-enabled/jenkins-proxy' do
    to '/etc/nginx/sites-available/jenkins-proxy'
end

service 'nginx' do
    action [:enable, :start]
end
