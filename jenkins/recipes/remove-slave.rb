bash 'remove_slave' do
  cwd "/tmp"
  code <<-EOH
  jenkins-cli -s http://internal-dse-jenkins-1387347571.us-east-1.elb.amazonaws.com delete-node #{node[:opsworks][:instance][:hostname]}
  EOH
end
