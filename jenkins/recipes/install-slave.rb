include_recipe 'jenkins::prereqs'

package 'jenkins-cli'

user "jenkins" do
    home "/home/jenkins"
    action :create
end

directory "/home/jenkins" do
    owner "jenkins"
    group "jenkins"
    mode "0777"
end

directory '/mnt/var/run/jenkins' do
    mode 0777
    owner 'jenkins'
    group 'jenkins'
    action :create
    recursive true
end

directory '/mnt/var/log/jenkins' do
    mode 0777
    owner 'jenkins'
    group 'jenkins'
    action :create
    recursive true
end

bash 'get_slave_jar' do
  cwd "/mnt/var/run/jenkins"
  code <<-EOH
    wget -qO slave.jar http://internal-dse-jenkins-1387347571.us-east-1.elb.amazonaws.com/jnlpJars/slave.jar
    chmod 755 slave.jar
    chown jenkins:jenkins slave.jar
  EOH
end

template '/etc/default/jenkins-slave' do
    source 'jenkins-slave.erb'
    mode 0777
    owner 'jenkins'
    group 'jenkins'
end

template '/etc/init/jenkins-slave.conf' do
    source 'jenkins-slave.conf.erb'
    mode 0777
    owner 'jenkins'
    group 'jenkins'
end

directory '/home/jenkins/.m2' do
    mode '0777'
    owner 'jenkins'
    group 'jenkins'
    action :create
    recursive true
end

