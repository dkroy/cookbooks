package "openjdk-7-jdk"
package 'nginx'
package 'git'
package 'python-pip'
package 'libpq-dev'
package 'python-dev'
package 'alien'
package 'graphviz'
package 'curl'
package 'zip'
package 'maven'
package 'libncurses5-dev'
package 'libffi-dev'
package 'firefox'
package 'xvfb'
package 'libmysqlclient-dev'

bash 'install_node' do
  cwd '/tmp'
  code <<-EOH
  curl -sL https://deb.nodesource.com/setup | sudo bash -
  apt-get install -yq nodejs
  npm install -g grunt
  npm install -g grunt-cli
  npm install -g gulp
  EOH
end 

bash 'install_awscli' do
  cwd "/tmp"
  code <<-EOH
    pip install awscli
  EOH
end

bash 'install_python_deps' do
  cwd '/tmp'
  code <<-EOH
    pip install virtualenv psycopg2==2.5.2
  EOH
end

bash 'remove_java_6' do
  cwd '/tmp'
  code <<-EOH
  apt-get remove -yq openjdk-6-*
  EOH
end

