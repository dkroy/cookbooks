bash 'start_slave' do
  cwd "/tmp"
  code <<-EOH
  initctl start jenkins-slave 
  EOH
end