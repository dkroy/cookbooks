directory '/mnt/var/lib/jenkins/updates' do
    mode '0755'
    owner 'jenkins'
    group 'jenkins'
    action :create
    recursive true
end

bash 'manually_update_jenkins_data' do
  cwd "/tmp"
  code <<-EOH
  wget http://updates.jenkins-ci.org/update-center.json -qO- | sed '1d;$d' > /mnt/var/lib/jenkins/updates/default.json
  chown jenkins:jenkins /mnt/var/lib/jenkins/updates/default.json
  chmod 755 /mnt/var/lib/jenkins/updates/default.json
  EOH
end

bash 'install_jenkins_plugins' do
  cwd "/tmp"
  user "jenkins"
  code <<-EOH
  jenkins-cli -s http://localhost:8080 install-plugin awseb-deployment-plugin git-client violation-columns matrix-auth maven-plugin cobertura delivery-pipeline-plugin javadoc external-monitor-job ant ssh-slaves simple-theme-plugin envinject build-pipeline-plugin pam-auth windows-slaves plot ssh-agent build-monitor-plugin  ssh-credentials skip-certificate-check scm-api parameterized-trigger subversion build-failure-analyzer copyartifact ldap htmlpublisher violations junit greenballs mailer buildgraph-view conditional-buildstep token-macro git naginator jquery-ui credentials translation shiningpanda text-finder slack jquery downstream-buildview github join matrix-project build-flow-plugin github-api cvs claim s3 run-condition antisamy-markup-formatter view-job-filters text-finder
  EOH
end

service 'jenkins' do
    action [:enable, :restart]
end
