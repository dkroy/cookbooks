#-*- encoding : utf-8 -*-
maintainer       "Dylan Roy"
maintainer_email ""
license          "Apache"
description      "Installs Jenkins"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.1.0"
name             "jenkins"