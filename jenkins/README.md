# Jenkins Recipe
Installs Jenkins

## Post-Installation (for master AND slaves)
Create the file `/etc/profile.d/aws_credentials.sh` with the following contents:

```
export AWS_ACCESS_KEY_ID=<key id>
export AWS_SECRET_ACCESS_KEY=<secret>
```

Set these same values in /etc/environment so that all users have access, regardless of distro:

```
echo 'AWS_ACCESS_KEY_ID=<key_id>' >> /etc/environment
echo 'AWS_SECRET_ACCESS_KEY=<secret>' >> /etc/environment
```

Download the djca-2048.pem file from http://pki.dowjones.com/

Copy the file to /home/ubuntu:
```
scp -i ~/insights-key.pem djca-2048.pem ubuntu@10.201.129.86:/home/ubuntu
```

SSH into the box.

Move and rename the file:
```
sudo mv /home/ubuntu/djca-2048.pem /usr/local/share/ca-certificates/djca-2048.crt
```

Change mode on file:

```
sudo chmod 777 /usr/local/share/ca-certificates/djca-2048.crt
```

Update the ca-certificates:

```
sudo update-ca-certificates
```

And restart jenkins:
```
/etc/init.d/jenkins restart
```

##Google keys
You'll also want to copy our djomniture.p12 and djomniture.pem from some undefined SOOPER secure  location to /var/lib/jenkins/

```
sudo su jenkins
cd /var/lib/jenkins
aws s3 cp s3://dj-skynet/development/keys/ . --recursive
```

##Refreshing configuration and jobs from repository
You'll need to do this manually if/whenever the master goes down or you decide to create a new one.

Create a key that Jenkins will be using.  You'll need to make sure there's a proper ssh certificate.  SSH to the new master, and create a new one:
1.
```
sudo su jenkins
cd ~/.ssh
ssh-keygen -t rsa -C "some_string_label" 
```

2.  Keep the defaults, the current plugin doesn't let you specify a keyfile path, it just uses id_rsa.
3.  To make things easy just leave an empty passphrase.

Now you've gotta tell github about that key so jenkins sync plugin can connect:
1.  Go to https://github.dowjones.net/settings/, then -> Add SSH Key
2.  Give it a meaningful name, and cat the contents of id_rsa.pub into it.  Should look something like this:
```
ssh-rsa AAAAB3NzaC1yc2EAAAamCeZq/OMliA+fbLluM/BGDgumd+4ZdkPaC6Q54092JYL4kBWpnG+ggNrqcO/FVyf2rjtOXWkjaP2UOkzwreewvybO+NPF3HVvmPsqPDLOF/AlKFcnxJsPq7bDHINiCRdwazpoe7P4gEtbvWoXl5s476NgXCbXaF2AiTm2dA/EpfFJk+4MHJ95LU14enYjw3RoqOWvp3l8YM0rsb0DZ7WXgreXVITY8b some_string_label
```
3.  Now github knows about the key jenkins plans on using to sync to it.

Make sure you test all the ssh goodness and make sure it works
```
sudo su jenkins
ssh -T git@github.dowjones.net
```

Should see a message saying you've connected.

To restore jenkins from the backup.


```
sudo su jenkins
cd /tmp
git clone https://github.dowjones.net/DataScience/jenkins-backup
cp -Ri jenkins-backup/ /mnt/var/lib
exit
sudo chown -R jenkins /mnt/var/lib/jenkins
```

then restart

```
sudo service jenkins restart
```

NOTE:  if you're having some weird issues with the sync, you can add a logger so you can see the error messages from the sync plugin

1.  Go to Manage Jenkins -> System Log
2.  Add New Log Recorder
3.  Craete a logger called "hudson.plugins.scm_sync_configuration" and that will keep all the logs from the sync plugin for you.

##Docs
https://wiki.jenkins-ci.org/display/JENKINS/Starting+and+Accessing+Jenkins